# LLAI Deploy Sandboxed

This repository constains the instructions to deploy the main components
of the _Low Latency Alert generation Infrastructure_ (LLAI) in a local Kubernetes instance.
The components taken into account are:
- [GraceDB](https://git.ligo.org/computing/gracedb/server/) 
- the [SCiMMA](https://scimma.org) [Hopskotch](https://my.hop.scimma.org/hopskotch) server
- the [Mock Event Generator](https://git.ligo.org/emfollow/mock-event-generator) 
- [GWCelery](https://git.ligo.org/emfollow/gwcelery)

Documentation is [here](https://emfollow.docs.ligo.org/k8s/llai-deploy-sandboxed/index.html).