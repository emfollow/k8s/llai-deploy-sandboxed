#!/bin/sh

x509_file=$1
netrc_file=$2
auth_file=$3
kafka_credential_file=$4
namespace=$5

if [ ! $x509_file ]; then
    echo "USAGE: add-gwcelery-secrets.sh x509_file [netrc_file auth_file kafka_credential_map_file namespace]"
    echo ""
    exit
fi

VOID_TMP_FILE=/tmp/add-gwcelery-secrets.void
if [ ! $netrc_file ]; then
    echo "" > ${VOID_TMP_FILE}
    netrc_file=${VOID_TMP_FILE}
fi

if [ ! $auth_file ]; then
    echo "" > ${VOID_TMP_FILE}
    auth_file=${VOID_TMP_FILE}
fi

if [ ! $kafka_credential_file ]; then
    echo "" > ${VOID_TMP_FILE}
    kafka_credential_file=${VOID_TMP_FILE}
fi

if [ ! $namespece ]; then
    namespace="default"
fi

kubectl create secret generic -n ${namespace} gwcelery-secrets \
  --from-file=auth.toml=${auth_file} \
  --from-file=netrc=${netrc_file} \
  --from-file=x509up_u1000=${x509_file} \
  --from-file=kafka_credential_map=${kafka_credential_file}

RETURN_STATUS=$?

if [ ${RETURN_STATUS} != 0 ]; then
    echo To delete the existing secret: kubectl delete secret  -n ${namespace} gwcelery-secrets
fi
