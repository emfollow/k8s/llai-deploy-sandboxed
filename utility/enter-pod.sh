#!/bin/sh

app=$1
namespace=$2
container=$3

if [ ! $app ]; then
    #  app="meg"
    echo "USAGE: ./enter-pod.sh PODNAME [NAMESPACE] [CONTAINER]"
    echo ""
    echo "Examples:"
    echo "  ./enter-pd.sh meg"
    echo "  ./enter-pd.sh gracedb"
    echo "  ./enter-pd.sh gracedb-0 default gracedb"
    echo "  ./enter-pd.sh gracedb llai-gracedb-test01"
    echo ""
    exit
fi

if [ ! $namespace ]; then

    pod=`kubectl get pods -n default | grep $app | head -n 1 | awk '{print $1}'`
    [ -z "$pod" ] && echo "ERR: no such pod" && exit 1

    echo "-----------------------------------------------------------"
    echo "COMMAND: kubectl exec -it ${pod} -c \"${container}\" -- bash"
    echo "- Pod name: $pod"
    echo "- Container name: $container"
    echo "-----------------------------------------------------------"
    echo "Entering app: $app in the default name space"

    kubectl exec -it $pod -c "$container" -- bash
else
    pod=`kubectl get pods -n $namespace | grep $app | head -n 1 | awk '{print $1}'`
    [ -z "$pod" ] && echo "ERR: no such pod" && exit 1

    echo "-----------------------------------------------------------"
    echo "COMMAND: kubectl exec -n ${namespace} -it $pod -c \"${container}\" -- bash"
    echo "- Pod name: $pod"
    echo "- Container name: $container"
    echo "- Namespace: $namespace"
    echo "-----------------------------------------------------------"
    echo "Entering app: $app"
    kubectl exec -n $namespace -it $pod -c "$container" -- bash
fi
