#!/bin/sh

image=$1
deployment=$2
namespace=$3

if [ ! $image ]; then
    echo "USAGE: ./deploy-new-gracedb-image.sh image [deployment] [namespace]"
    echo " image      : the image to be deployed"
    echo " deployment : the deployment to updata [gracedb]"
    echo " namespace  : the k8s namespace  [default]"
    echo "EXAMPLE: ./deploy-new-gracedb-image.sh containers.ligo.org/computing/gracedb/server:gracedb-2.28.2"
    exit
fi
if [ ! $deployment ]; then
    deployment="gracedb"
fi
if [ ! $namespace ]; then
    namespace="default"
fi

echo "-----------------------------------------------------------"
echo "Applying command:"
echo "helm upgrade -n ${namespace} --install --reuse-values \\"
echo "     --set gracedb.image=\"${image}\" \\"
echo "     ${deployment} gracedb-helm/gracedb"
echo "-----------------------------------------------------------"
helm upgrade -n ${namespace} --install --reuse-values \
     --set gracedb.image="${image}" \
     ${deployment} gracedb-helm/gracedb
