#!/usr/bin/env sh

namespace=$1

if [ ! $namespace ]; then
    echo " ------------------------------------------------------------------------------"
    echo " UTILITY: ./get_gracedb_ca namespace"
    echo ""
    echo " Get certification autority (from k8s secret: gracedb-ca) and save it to ca.crt"
    echo " and create a new ca certificate bundle and save it to cacerts.pem."
    echo " The ca.cert file may be add to meg with the comand: meg ca-certificate ca.crt"
    echo ""
    echo "  USAGE: Define the env variable"
    echo "    export LOCAL_CA=`pwd`/cacerts.pem"
    echo ""
    echo "  Here some example on its usage to overdrive the defalut ca certificate autority"
    echo ""
    echo "    REQUESTS_CA_BUNDLE=\${LOCAL_CA} gracedb -s https://gracedb.default.svc.cluster.local/api credentials server"
    echo "    REQUESTS_CA_BUNDLE=\${LOCAL_CA} meg validate --source local S241002a"
    echo ""
    echo "  To copy secret from default namespace to a new_namespace"
    echo "    kubectl get secret gracedb-ca --namespace=default -o yaml | \\"
    echo "         | sed 's/namespace: .*/namespace: new_namespace/' \\"
    echo "         | kubectl apply -f -"
    echo ""
    echo " certifi: provides Mozilla’s carefully curated collection of Root Certificates "
    echo " for validating the trustworthiness of SSL certificates while verifying the"
    echo " identity of TLS hosts  [cat \$(python3 -m certifi) > root_cacerts.crt]"
    echo " ---------------------------------------------------------------------------"
    exit
fi

kubectl get secrets gracedb-ca --namespace ${namespace} -o jsonpath='{.data.ca\.crt}' | base64 -d > ca.crt
cat $(python3 -m certifi) ca.crt > cacerts.pem
