# Quickstart on LDAS VMs

For a quick deployment of the LLAI on K3s on a _fluxuser_ machine at CIT, follow the brief instructions below. Additional details can be found in the remainder of this documentation.

## Before you begin 

Activate an IGWN Conda environment: 
```
source /cvmfs/software.igwn.org/conda/etc/profile.d/conda.sh
```
```
conda activate igwn
```

Get a personal proxy certificate:
```
ligo-proxy-init -H 8760 <marie.curie>
```

Make sure that you have a _read_api_ scoped [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

Clone the [LLAI Deploy Sandboxed](https://git.ligo.org/emfollow/k8s/llai-deploy-sandboxed) GitLab project:
```
git clone https://git.ligo.org/emfollow/k8s/llai-deploy-sandboxed.git
```
```
cd llai-deploy-sandboxed
```

## Installation 

Add the relevant Helm repositories:
```
helm repo add --username <marie.curie> --password <read-api-token> --force-update gracedb-helm https://git.ligo.org/api/v4/projects/15655/packages/helm/stable
```
```
helm repo add --username <marie.curie> --password <read-api-token> --force-update gwcelery-helm https://git.ligo.org/api/v4/projects/16068/packages/helm/stable
```

Create a Secret for your proxy certificate:
```
./utility/add-gwcelery-secret.sh /tmp/x509up_u<uid>
```

Install the Hopskotch server:
```
helm  upgrade --install --set storageClassName=local-path hopskotch gracedb-helm/hopskotch 
```

Install GraceDB (when all the Hopskotch Pods are ready):
```
 helm  upgrade --install \
        --set storageClassName=local-path \
        --set traefik.install=false \
        --set publicName="gracedb-dev<X>.ldas.cit" \
        gracedb gracedb-helm/gracedb
```

Install GWCelery (when all the GraceDB Pods are ready):
```
helm  upgrade --install --set gwcelery.storageClassName=local-path \
        --set gwcelery.ingress.host=fluxuser<X>.ldas.cit \
        --set gwcelery.gracedb.hostname=gracedb-dev<X>.ldas.cit
        gwcelery gwcelery-helm/gwcelery
```

## Access to web endpoints

The installed services are available on the local network only. You might want to use a SOCKS proxy to access these services from your local browser as explained [here](gracedb-deploy.md#access-the-local-gracedb-instance).

GraceDB can be reached at:
```
https://gracedb-dev<X>.ldas.cit
```

Flower and the GWCelery console are available at:
```
http://fluxuser<X>.ldas.cit/flower/
```
```
http://fluxuser<X>.ldas.cit/gwcelery/
```

## Configuration
Add yourself as admin user of GraceDB:
```
ALL_PERMS=True ./utility/add-account.sh <marie.curie>
```

In order to grant full privileges to your user, access the admin interface at the URL:
``` 
https://gracedb-dev<X>.ldas.cit/admin
```
The default login credentials are _admin/mypassword_.

Navigate to **AUTHENTICATION AND AUTHORIZATION->Users**, search for your account, choose all the available groups and permissions and save.

## Test

You are now ready to [install and use MEG for testing](meg.md). 

