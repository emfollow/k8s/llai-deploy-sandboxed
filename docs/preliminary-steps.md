# Preliminary steps

!!! attention
    K3s, kubectl and Helm are already installed and configured on the LDAS _fluxuser_ machines, so this section can be skipped if you are using those resources. The _ligo-proxy-init_ command is available by activating an IGWN Conda environment: 
    ```
    user> source /cvmfs/software.igwn.org/conda/etc/profile.d/conda.sh
    user> conda activate igwn
    ```

## Setup the Kubernetes cluster

If you are utilizing your own resources, you should follow the official documentation to install the chosen Kubernetes distribution (you need to have admin privileges on the system).

!!! warning 
    Please note that installing both K3s and minikube on the same system might cause conflicts.

=== "K3s"
    Installation instructions for K3s can he found [here](https://docs.k3s.io/quick-start).

    The simplest installation process can be summarized by executing the following command:
    ```
    root> curl -sfL https://get.k3s.io | sh -
    ```
    (this procedure also installs the _kubectl_ CLI).

    As privileged user, copy the default configuration file to your home directory:
    ```
    root> mkdir /home/<username>/.kube
    root> cp /etc/rancher/k3s/k3s.yaml /home/<username>/.kube/config
    root> chown -R <username>:<username> /home/<username>/.kube
    ```
    Add the path to the configuration file to your `.bash_profile`:
    ```
    user> echo "export KUBECONFIG=~/.kube/config" >> ~/.bash_profile
    user> source ~/.bash_profile
    ```

    

=== "minikube"
    Installation instructions for minikube can be found [here](https://minikube.sigs.k8s.io/docs/start/?arch=%2Fmacos%2Fx86-64%2Fstable%2Fbinary+download).
    
    The simplest installation process can be summarized by executing the following commands:
    ```
    root> curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-<operating_system>-<architecture>
    root> sudo install minikube-<operating_system>-<architecture> /usr/local/bin/minikube
    ```

    After installing minikube, you need to initialize a default cluster (which will be called _minikube_):
    ```
    user> minikube start 
    ```
    To check the status of your newly created cluster (or profile):
    ```
    user> minikube profile list
    |----------|-----------|---------|--------------|------|---------|---------|-------|
    | Profile  | VM Driver | Runtime |      IP      | Port | Version | Status  | Nodes |
    |----------|-----------|---------|--------------|------|---------|---------|-------|
    | minikube | docker    | docker  | 192.168.49.2 | 8443 | v1.23.3 | Running |     1 |
    |----------|-----------|---------|--------------|------|---------|---------|-------|
    ```

    The installation  procedure will not install the _kubectl_ CLI, which should be installed following [these instructions](https://kubernetes.io/docs/tasks/tools/).

    !!! warning "Installation on macOS with Apple Silicon"
        Please note that all the Docker images produced by the LVK collaboration are based on the 
        x86-64 architecture and need _Rosetta_. During the minikube installation, select the x86-64 architecture.
 


You should now be able to contact the Kubernetes cluster as unprivileged user:
```
user> kubectl get pods 
No resources found in default namespace.
```

## Required software    
The following packages should be installed in your system:

- [Helm](https://helm.sh/docs/intro/install/)  >= 3.9.0

- [ligo-proxy-init](https://computing.docs.ligo.org/guide/auth/x509/#install-ligo-proxy-init)

Moreover, you should clone the [LLAI Deploy Sandboxed](https://git.ligo.org/emfollow/k8s/llai-deploy-sandboxed) GitLab project, that contains some necessary utilities.