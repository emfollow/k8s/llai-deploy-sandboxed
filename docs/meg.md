# Mock Event Generator
The [Mock Event Generator](https://git.ligo.org/emfollow/mock-event-generator) (MEG) is a tool that allows you to re-upload past GW events to a given GraceDB instance. 

## Installation 
We provide a simple Helm chart to install MEG on your Kubernetes cluster. The chart allows the following configuraion parameters:

| Key     | Description | Type | Default |
| ------------------ | ------------------------------------------ | ------ | ---------- |
| `meg.storageClassName` | Storage class name for persistent storage. | string | "standard" |
| `meg.image`| Docker image and tag.| string | "containers.ligo.org/emfollow/mock-event-generator:1.4.14" |
| `meg.gracedb.caSecret`| Name of the Secret containing the CA certificate of your local graceDB instance. | string | "gracedb-ca" |
| `meg.gracedb.hostAliases`| Hostname/IP [mapping](https://kubernetes.io/docs/tasks/network/customize-hosts-file-for-pods/) for GraceDB. | list | - ip: 10.100.100.10 <br> &nbsp; hostnames:  <br>  &nbsp;&nbsp;&nbsp; - gracedb.default.svc.cluster.local|

To install the chart:

=== "K3s"
    ```
    helm upgrade --install -n default \
    --set meg.storageClassName=local-path \
    --set meg.gracedb.hostAliases=false \
    meg ./meg
    ```

=== "minikube"
    ```
    helm upgrade --install -n default \
    meg ./meg
    ```

## Event generation

In order to use MEG, enter the `meg-0` Pod and get a proxy certificate to interact with your GraceDB instance:
```
user> kubectl exec -it meg-0 -- bash 
meguser@myhost:~$ ligo-proxy-init <marie.curie>
```

Please refer to the [MEG documentation](https://emfollow.docs.ligo.org/mock-event-generator/usage.html) for more information on how to replay events. We provide only some useful example here. 

### Create single events

In order to re-crate all G-events belonging to a given Superevent present in the production GraceDB instance, run the following command: 
```
meg create --source production --target https://<graceDB-server-name>/  --original-search S240428dr
```
where `<graceDB-server-name>` is, in the case of the LDAS `fluxuser` machines:
```
grecedb-dev<X>.ldas.cit
```

or, in all other cases:
```
gracedb.default.svc.cluster.local
```

In the minikube case, a tunnel should be opened on a separate teminal if you did not do it already:
```
minikube tunnel
```

The `meg create` command should give you a similar output:
```
2025-01-23 08:43:59 INFO     Replay of S240428dr using provided gps time 1421657057.556796 (now()=1421657057.57132)
2025-01-23 08:43:59 INFO     The uploads to GraceDB will take 83.94s, like the original schedule. (full 85.94s)
2025-01-23 08:43:59 INFO     G-event G478660 with delay of 83.94s (83.94)
2025-01-23 08:43:59 INFO     G-event G478659 with delay of 78.65s (78.65)
2025-01-23 08:43:59 INFO     G-event G478658 with delay of 22.71s (22.71)
2025-01-23 08:43:59 INFO     G-event G478657 with delay of 21.72s (21.72)
2025-01-23 08:43:59 INFO     G-event G478656 with delay of 13.56s (13.56)
2025-01-23 08:43:59 INFO     G-event G478655 with delay of 12.70s (12.70)
2025-01-23 08:44:12 INFO     Created G478655 -> G0346  gstlal       CBC            AllSky         1421657057.557223
2025-01-23 08:44:13 INFO     Created G478656 -> G0347  gstlal       CBC            AllSky         1421657057.557337
2025-01-23 08:44:21 INFO     Created G478657 -> G0348  gstlal       CBC            AllSky         1421657057.557337
2025-01-23 08:44:21 INFO     Uploaded file for event G0347: gstlal.p_astro.json,0
2025-01-23 08:44:21 INFO     Written label for event G0347: PASTRO_READY
2025-01-23 08:44:22 INFO     Created G478658 -> G0349  gstlal       CBC            AllSky         1421657057.557223
2025-01-23 08:44:22 INFO     Uploaded file for event G0348: gstlal.p_astro.json,0
2025-01-23 08:44:22 INFO     Written label for event G0348: PASTRO_READY
2025-01-23 08:44:32 INFO     Uploaded file for event G0346: gstlal.p_astro.json,0
2025-01-23 08:44:32 INFO     Written label for event G0346: PASTRO_READY
2025-01-23 08:44:33 INFO     Uploaded file for event G0349: gstlal.p_astro.json,0
2025-01-23 08:44:33 INFO     Written label for event G0349: PASTRO_READY
2025-01-23 08:45:18 INFO     Created G478659 -> G0350  gstlal       CBC            AllSky         1421657057.556796
2025-01-23 08:45:22 INFO     Uploaded file for event G0350: gstlal.p_astro.json,0
2025-01-23 08:45:22 INFO     Written label for event G0350: PASTRO_READY
2025-01-23 08:45:24 INFO     Created G478660 -> G0351  gstlal       CBC            AllSky         1421657057.556796
2025-01-23 08:45:26 INFO     Uploaded file for event G0351: gstlal.p_astro.json,0
2025-01-23 08:45:26 INFO     Written label for event G0351: PASTRO_READY
```

<!---
### MDC replay
TODO
-->

## Uninstall

```
helm uninstall meg
kubectl delete pvc meg-data-meg-0
```