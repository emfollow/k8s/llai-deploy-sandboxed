# Import a custom CA in Firefox

Open the Firefox application and from the topmost bar select: **Firefox > Preferences**.

![](images/add_ca_firefox_1.png)

Select the **Privacy & Security** tab from the left side menu and scroll down to the **Certificates** section. Click the **View certificates** button.
![](images/add_ca_firefox_2.png)

From the **Authorities** tab click the **Import** button and select the local `ca.crt` file.

![](images/add_ca_firefox_3.png)

Check the _Trust this CA to identify websites_ box.

![](images/add_ca_firefox_4.png)
